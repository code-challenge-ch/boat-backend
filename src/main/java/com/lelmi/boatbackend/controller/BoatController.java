package com.lelmi.boatbackend.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lelmi.boatbackend.dto.BoatDto;
import com.lelmi.boatbackend.model.Boat;
import com.lelmi.boatbackend.model.User;
import com.lelmi.boatbackend.service.BoatService;
import com.lelmi.boatbackend.service.UserService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class BoatController {
	
	@Autowired
	private BoatService boatService;
	
	@Autowired
	private UserService userService;
	

    @GetMapping("/boats")
    List<Boat> all() {
        return this.boatService.getAllBoats();
    }
    
    @GetMapping("/boat/{id}")
    public ResponseEntity<Boat> getBoatById(@PathVariable("id") long id) {
      Optional<Boat> boat = this.boatService.getBoat(id);

      if (boat.isPresent()) {
        return new ResponseEntity<>(boat.get(), HttpStatus.OK);
      } else {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
    }
    
    @PostMapping("/boat")
	public ResponseEntity<Boat> createBoat(@Valid @RequestBody BoatDto boatDto){
		Boat boat = boatDto.toBoat();
		User user = this.userService.getUser(boatDto.getUser().getId());
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND); 
		}
		boat.setUser(user);
		return new ResponseEntity<>(this.boatService.saveBoat(boat), HttpStatus.CREATED);
	}
    
    
    @DeleteMapping("/boat/{id}")
    public ResponseEntity<HttpStatus> deleteBoat(@PathVariable("id") long id) {
      try {
    	 this.boatService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
      } catch (Exception e) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
    }
    
    @PutMapping("/boat/{id}")
    public ResponseEntity<Boat> updateBoat(@PathVariable("id") long id, @Valid @RequestBody BoatDto boatDto) {
      Optional<Boat> boatToUpdate = this.boatService.getBoat(id);

      if (boatToUpdate.isPresent()) {
    	  Boat boat = boatDto.toBoat();
    	  boat.setId(id);
    	  boat.setUser(boatDto.getUser());
        return new ResponseEntity<>(this.boatService.saveBoat(boat), HttpStatus.OK);
      } else {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
    }

}
