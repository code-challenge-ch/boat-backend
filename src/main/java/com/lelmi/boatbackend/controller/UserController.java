package com.lelmi.boatbackend.controller;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lelmi.boatbackend.dto.RegisterUserDto;
import com.lelmi.boatbackend.jwt.JwtTokenProvider;
import com.lelmi.boatbackend.model.Role;
import com.lelmi.boatbackend.model.User;
import com.lelmi.boatbackend.service.UserService;;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class UserController {
	
	@Autowired
	private JwtTokenProvider tokenProvider;
	
	@Autowired
	private UserService userService;
	
	@PostMapping("/user/register")
	public ResponseEntity<?> register(@Valid @RequestBody RegisterUserDto registerUserDto){
		
		if (this.userService.getUserByUsername(registerUserDto.getUsername()) != null) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
		User user = registerUserDto.toUser();
		user.setRole(Role.USER);
		return new ResponseEntity<>(this.userService.saveUser(user), HttpStatus.CREATED);
	}
	
	@GetMapping("/user/login")
	public ResponseEntity<?> login(Principal principal){
		if (principal == null || principal.getName() == null) {
			return ResponseEntity.ok(principal);
		}
		
		UsernamePasswordAuthenticationToken authenticationtoken = (UsernamePasswordAuthenticationToken) principal;
		User user = this.userService.getUserByUsername(principal.getName());
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND); 
		}
		user.setToken(tokenProvider.generateToken(authenticationtoken));
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

}
