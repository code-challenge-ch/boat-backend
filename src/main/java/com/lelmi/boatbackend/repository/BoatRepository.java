package com.lelmi.boatbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lelmi.boatbackend.model.Boat;

@Repository
public interface BoatRepository extends JpaRepository<Boat, Long> {

}
