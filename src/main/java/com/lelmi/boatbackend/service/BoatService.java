package com.lelmi.boatbackend.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lelmi.boatbackend.model.Boat;
import com.lelmi.boatbackend.repository.BoatRepository;

@Service
public class BoatService {

	@Autowired
    private BoatRepository boatRepository;

    public Optional<Boat> getBoat(Long id) {
        return this.boatRepository.findById(id);
    }

    public Boat saveBoat(Boat boat){
        return this.boatRepository.save(boat);
    }

    public List<Boat> getAllBoats() {
        return this.boatRepository.findAll();
    }

    public void deleteById(Long id) {
        this.boatRepository.deleteById(id);
    }
}
