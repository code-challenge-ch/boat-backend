package com.lelmi.boatbackend.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.lelmi.boatbackend.model.User;
import com.lelmi.boatbackend.repository.UserRepository;

import javassist.NotFoundException;

@Service
public class UserService {

	@Autowired
    private UserRepository userRepository;
	
	@Autowired
    private PasswordEncoder passwordEncoder;

    public User getUser(Long id) {
        return this.userRepository.getById(id);
    }
    
    public User getUserByUsername(String username) {
        return this.userRepository.findByUsername(username);
    }

    public User saveUser(User user){
    	user.setPassword(passwordEncoder.encode(user.getPassword()));
        return this.userRepository.save(user);
    }

    public List<User> getAllUsers() {
        return this.userRepository.findAll();
    }

    public void deleteById(Long id) {
        this.userRepository.deleteById(id);
    }
}
