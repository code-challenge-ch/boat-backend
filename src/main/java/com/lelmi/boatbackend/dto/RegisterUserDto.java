package com.lelmi.boatbackend.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.lelmi.boatbackend.model.User;

public class RegisterUserDto {
	
	@NotEmpty(message = "The full name is required.")
	@Size(min = 2, max = 100, message = "The length of full name must be between 2 and 100 characters.")
	private String name;

	@NotEmpty(message = "The username is required.")
	@Size(min = 2, max = 100, message = "The length of username must be between 2 and 100 characters.")
	private String username;
	
	@NotEmpty(message = "The password is required.")
	@Size(min = 8, max = 100, message = "The length of password must be between 2 and 100 characters.")
	private String password;

	public User toUser() {
		User user = new User ();
		user.setName(name);
		user.setUsername(username);
		user.setPassword(password);
		return user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
