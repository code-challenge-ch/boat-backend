package com.lelmi.boatbackend.dto;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.lelmi.boatbackend.model.Boat;
import com.lelmi.boatbackend.model.User;


public class BoatDto {

	@NotEmpty(message = "The boat name is required.")
	@Size(min = 2, max = 100, message = "The length of boat name must be between 2 and 100 characters.")
	private String name;
	
	@NotEmpty(message = "The boat description is required.")
	@Size(min = 2, max = 100, message = "The length of boat description must be between 2 and 100 characters.")
	private String description;
	
	@DecimalMin(value = "0.0", inclusive = true)
	private int price;
	
	@Size(min = 2, max = 100, message = "The length of boat model must be between 2 and 100 characters.")
	private String model;
	
	@Size(min = 2, max = 100, message = "The length of boat location must be between 2 and 100 characters.")
	private String location;

	private User user;

	public Boat toBoat() {
		Boat boat = new Boat ();
		boat.setName(name);
		boat.setDescription(description);
		boat.setPrice(price);
		boat.setModel(model);
		boat.setLocation(location);
		return boat;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	

}
