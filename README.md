This was a recent code challenge which I had to do. I could spent about 10 hours on it.
The requirements were as following:


- The user opens your app and gets a login screen. He has to login. After a successful login he is navigated to the overview page (UC2).
- The user has a list of all boat resources
- The user can add or update or delete boat
- The user clicks on a boat item and gets a detail view over it.


The back-end is RESTFUL api with specific requirements :


- Create an CRUD endpoint for managing boat. A boat should have at least the following attributes and appropriate validation:
• Name
• Description

REQ2 (optional)

- Implement the following security aspects.
• Authentication / Authorization (only authenticated user can access to the resources )
